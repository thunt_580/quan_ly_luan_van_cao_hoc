package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import play.data.Form;
import models.Student;
import java.util.*;
import views.html.students.details;
import views.html.students.list;

public class Students extends Controller{
    private static final Form<Student> studentForm = Form.form(Student.class);
    public static Result list() {
        List<Student> students = Student.findAll();
        return ok(list.render(students));
    }
    public static Result details(Student student) {
       /* final Student student = Student.findByMSV(msv);*/
        if(student == null) {
            return notFound(String.format("Student %s does not exist.",student.msv));
        }
        Form<Student> filledForm = studentForm.fill(student);
        return ok(details.render(filledForm));
    }
    public static Result newStudent() {
        return ok(details.render(studentForm));
    }
    public static Result save() {
        Form<Student> boundForm = studentForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(details.render(boundForm));
        }
        Student student =boundForm.get();
        student.save();
        flash("success", String.format("Successfully added student %s", student));
        return redirect(routes.Students.list());
    }
    public static Result delete(String msv) {
        final Student student = Student.findByMSV(msv);
        if(student == null) {
            return notFound(String.format("Student %s does not exists.",msv));
        }
        Student.remove(student);
        return redirect(routes.Students.list());
    }
}