package controllers;

import play.*;
import play.mvc.*;
import static play.data.Form.form;

import views.html.*;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }
}