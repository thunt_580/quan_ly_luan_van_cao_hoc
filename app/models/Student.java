package models;

import java.lang.Override;
import java.lang.String;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import play.data.validation.Constraints;
import play.mvc.PathBindable;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Student extends Model implements PathBindable<Student> {
    @Id
    public Long id;
    @Constraints.Required
    public String msv;
    @Constraints.Required
    public String name;
    @Constraints.Required
    public String birthday;
    @Constraints.Required
    public String address;
    @Constraints.Required
    public String major;
    @Constraints.Required
    public String course;
    public String phoneNumber;

    public Student(){};
    public Student(String msv, String name, String birthday, String address, String major, String course, String phoneNumber) {
        this.msv = msv;
        this.name = name;
        this.birthday = birthday;
        this.address = address;
        this.major = major;
        this.course = course;
        this.phoneNumber = phoneNumber;
    }
    private static  List<Student> students;
    static {
        students = new ArrayList<Student>();
        students.add(new Student("11111","nguyen van a","22/02/1995","thai binh","cntt","k58","012345678"));
        students.add(new Student("11112","nguyen van a","28/11/1995","thai nguyen","cntt","k58","0123756"));
        students.add(new Student("11113","nguyen van b","22/02/1995","ha noi","cntt","k58","0123456"));
    }
    public String toString() {
        return String.format("%s - %s - %s - %s - %s - %s - %s ",msv, name, birthday, address, major, course, phoneNumber);
    }
    public static List<Student> findAll() {
        return new ArrayList<Student>(students);
    }

    public static Student findByMSV(String msv) {
        for (Student candidate : students) {
            if(candidate.msv.equals(msv)) {
                return candidate;
            }
        }
        return null;
    }

    public static List<Student> findByName(String term) {
        final List<Student> results = new ArrayList<Student>();
        for(Student candidate : students) {
            if(candidate.name.toLowerCase().contains(term.toLowerCase())) {
                results.add(candidate);
            }
        }
        return results;
    }
    public static boolean remove(Student student) {
        return students.remove(student);
    }
    public void save() {
        students.remove(findByMSV(this.msv));
        students.add(this);
    }
    @Override
    public Student bind(String key, String value) {
        return findByMSV(value);
    }

    @Override
    public String unbind(String key) {
        return msv;
    }

    @Override
    public String javascriptUnbind() {
        return msv;
    }
}